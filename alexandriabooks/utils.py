import asyncio
import typing
import re
import datetime

from .protos.v1 import alexandriabooks_pb2, alexandriabooks_pb2_grpc

def parse_bool(bool_input: typing.Any) -> bool:
    if bool_input is None:
        return False

    if isinstance(bool_input, bool):
        return bool_input
    elif isinstance(bool_input, str):
        parse_bool = bool_input.lower()
        if not parse_bool:
            return False

        return parse_bool[0] in ['y', '1', 't']
    elif isinstance(bool_input, int):
        return bool_input != 0

    raise ValueError(f"Cannot make value {bool_input} a boolean")

def parse_date(date_input: typing.Any) -> alexandriabooks_pb2.Date:
    if date_input is None:
        return alexandriabooks_pb2.Date()
    
    if isinstance(date_input, str):
        match = re.match(r"([0-9][0-9][0-9][0-9])([0-9][0-9])?([0-9][0-9])?", date_input)
        if match:
            return alexandriabooks_pb2.Date(
                year=int(match.group(1) or "0"),
                month=int(match.group(2) or "0"),
                day=int(match.group(3) or "0")
            )
    elif isinstance(date_input, (datetime.date, datetime.datetime)):
        return alexandriabooks_pb2.Date(
            year=date_input.year,
            month=date_input.month,
            day=date_input.day
        )

    return alexandriabooks_pb2.Date()

def protobuf_config_to_dict(config: typing.List[alexandriabooks_pb2.ConfigValue]) -> typing.Dict[str, typing.Any]:
    ret_val = {}

    for value in config:
        value_field = value.WhichOneof('value')
        if value_field == "nested_value":
            ret_val[value.key] = protobuf_config_to_dict(value.nested_value.values)
        else:
            ret_val[value.key] = getattr(value, value_field)

    return ret_val

def merge_sorted_generators(is_left_before_right_func, *generators):
    tracking = {}
    for generator in generators:
        try:
            value = generator.__next__()
            tracking[generator] = value
        except StopIteration:
            pass

    while tracking:
        first = None
        first_generator = None
        for generator, value in tracking.items():
            if first is None or is_left_before_right_func(value, first):
                first = value
                first_generator = generator
        
        try:
            value = first_generator.__next__()
            tracking[first_generator] = value
        except StopIteration:
            del tracking[first_generator]
        
        yield first

async def merge_sorted_async_generators(is_left_before_right_func, *generators):
    results = list(await asyncio.gather(
        *[
            generator.__anext__()
            for generator in generators
        ],
        return_exceptions=True
    ))
    generators = list(generators)

    while generators:    
        first = None
        first_idx = None

        finished = []
        for idx, value in enumerate(results):
            if isinstance(value, Exception):
                finished.append(idx)
            elif first is None or is_left_before_right_func(value, first):
                first = value
                first_idx = idx

        if first is None:
            break

        try:
            results[first_idx] = await generators[first_idx].__anext__()
        except StopAsyncIteration:
            finished.append(first_idx)
        finished.sort()

        for idx in finished[::-1]:
            del results[idx]
            del generators[idx]
        
        yield first


class AsyncMetadataInterceptor:
    def __init__(self, plugin_host: alexandriabooks_pb2_grpc.PluginHostServiceStub, priority: int):
        self._plugin_host = plugin_host
        self._queue = asyncio.Queue()
        self._task = None
        self.priority = priority

    def start(self):
        self._task = asyncio.create_task(self._run())

    def stop(self):
        self._task.cancel()

    async def run_and_wait(self):
        await self._run()
    
    async def wait_for(self):
        await self._task

    async def _run(self):
        async for interception in self._plugin_host.SourceMetadataIntercepter(self._processed()):
            processed = await self.process(interception.metadata)
            validity = interception.validity
            if isinstance(processed, (list, tuple)):
                processed, validity = processed
                if validity is None:
                    validity = interception.validity
                elif isinstance(validity, bool):
                    validity = alexandriabooks_pb2.alexandriabooks_pb2.SOURCE_VALIDITY_VALID if validity else alexandriabooks_pb2.SOURCE_VALIDITY_INVALID
            await self._queue.put((processed, validity))

    async def _processed(self):
        yield alexandriabooks_pb2.SourceMetadataIntercepterRequest(
            subscription=alexandriabooks_pb2.MetadataInterceptSubscription(priority=self.priority)
        )

        while True:
            processed_metadata, validity = await self._queue.get()
            
            yield alexandriabooks_pb2.SourceMetadataIntercepterRequest(
                result=alexandriabooks_pb2.MetadataInterceptResult(
                    metadata=processed_metadata,
                    validity=validity
                )
            )

    async def process(self, metadata: alexandriabooks_pb2.SourceMetadata):
        return metadata, None


class AsyncStatusTracker:
    def __init__(self, plugin_host: alexandriabooks_pb2_grpc.PluginHostServiceStub, plugin_instance_id: str, status: alexandriabooks_pb2.Status = alexandriabooks_pb2.STATUS_INITIALIZING, details: str = "") -> None:
        self._plugin_host = plugin_host
        self._plugin_instance_id = plugin_instance_id
        self.status = status
        self.details = details
        self._status_event = asyncio.Event()
        self.host_status = alexandriabooks_pb2.STATUS_UNSPECIFIED
        self.host_details = ""
        self._host_status_event = asyncio.Event()
        self._running = False

    def start(self):
        self._running = True
        self._task = asyncio.create_task(self._run())

    def stop(self):
        self._running = False
        self._host_status_event.set()

    async def wait_for_host_ready(self):
        while self.host_status not in [alexandriabooks_pb2.STATUS_READY]:
            await self._host_status_event.wait()
            self._host_status_event.clear()
        return self.host_status

    async def update_status(self, status: alexandriabooks_pb2.Status, details: str = ""):
        self.status = status
        self.details = details
        self._status_event.set()

    async def _run(self):
        try:
            async for host_status_update in self._plugin_host.StatusUpdates(self._updates()):
                self.host_status = host_status_update.status
                self.host_details = host_status_update.details
                self._host_status_event.set()
        except Exception:
            pass
        self._running = False

    async def _updates(self):
        yield alexandriabooks_pb2.StatusUpdatesRequest(plugin_instance_id=self._plugin_instance_id, status=self.status, details=self.details)
        while self._running:
            await self._status_event.wait()
            self._status_event.clear()
            yield alexandriabooks_pb2.StatusUpdatesRequest(plugin_instance_id=self._plugin_instance_id, status=self.status, details=self.details)


class PeopleBuilder:
    def __init__(self) -> None:
        self._people = {}

    def add_person(self, *, full_name=None, given_names=None, last_name=None, roles=None):
        if given_names is not None:
            if isinstance(given_names, (list, tuple)):
                given_names = " ".join(given_names)

        if full_name is None:
            full_name = ""
            if given_names:
                full_name = given_names
            if last_name is not None:
                if full_name:
                    full_name += " "
                full_name += last_name

            if not full_name:
                return
        
        existing_roles = self._people.setdefault(full_name, (full_name, given_names, last_name, set()))[3]
        if isinstance(roles, int):
            roles = [roles]
        for role in roles:
            existing_roles.add(role)

    def get_people(self) -> typing.List[alexandriabooks_pb2.Person]:
        return list(
            alexandriabooks_pb2.Person(
                full_name=full_name,
                given_names=given_names,
                last_name=last_name,
                roles=list(roles)
            )
            for full_name, given_names, last_name, roles in self._people.values()
        )
