import logging

from .protos.v1 import alexandriabooks_pb2

log_level_names_to_proto_consts = {
    "DEBUG": alexandriabooks_pb2.LOG_LEVEL_DEBUG,
    "INFO": alexandriabooks_pb2.LOG_LEVEL_INFO,
    "WARNING": alexandriabooks_pb2.LOG_LEVEL_WARNING,
    "ERROR": alexandriabooks_pb2.LOG_LEVEL_ERROR,
}

log_level_proto_consts_to_names = {
    value:key
    for key, value in log_level_names_to_proto_consts.items()
}

log_level_names_to_python_consts = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
}
