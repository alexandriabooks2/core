# AlexandriaBooks Core

## Regenerating Protobuf files

Ensure buf is installed from https://github.com/bufbuild/buf/releases :

    buf generate https://gitlab.com/alexandriabooks2/protos.git

